import { Component, OnInit } from '@angular/core';
import{ Router,RouterModule,Routes} from '@angular/router';
import { StoreService } from '../store.service';
@Component({
  selector: 'app-show',
  templateUrl: './show.component.html',
  styleUrls: ['./show.component.css']
})
export class ShowComponent implements OnInit {
Value;
eyeicon:string="password";
  constructor(private route:Router ,private serviceFormData:StoreService) { 
   // this.Value=JSON.parse( localStorage.getItem("json"));
   this.Value=JSON.parse(this.serviceFormData.getData());
   console.log(this.Value);
  }
 

  
  editfunction(){
  this.route.navigate(['/formpage']);
}

showeyefunction(){
  console.log("g");
  if(this.eyeicon=="password"){
    this.eyeicon="text";
  }
  else{
    this.eyeicon="password";
  }
}
formsubmit(){
  if(this.Value){
    alert("FORM IS SUBMITTED");
  }
  else{
    alert("invalid submission");
  }
}
ngOnInit() {

}

}
