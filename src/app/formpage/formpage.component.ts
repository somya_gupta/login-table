import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import { Validators } from '@angular/forms';
import { FormControl , FormGroup } from '@angular/forms';
import { StoreService } from '../store.service';
import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';
@Component({
  selector: 'app-formpage',
  templateUrl: './formpage.component.html',
  styleUrls: ['./formpage.component.css']
})
export class FormPageComponent implements OnInit{
  update;
  Value;
  
  
  checkPass:boolean=false;
  check(){

    if(this.registerForm.value.password !== this.registerForm.value.confirmpassword)
    {
        this.checkPass=false;
    }
    else{
      this.checkPass=true;
    }
    console.log(this.checkPass);
  }
  onSubmit(){
    //localStorage.setItem("json", JSON.stringify(this.registerForm.value));
    
    this.route.navigate(['/show']);

    this.Value=JSON.stringify(this.registerForm.value);
    this.serviceFormData.storedata(this.Value);
    }

  

registerForm:FormGroup;
  constructor(private route:Router,private serviceFormData:StoreService) { 
  

this.registerForm=new FormGroup({
  firstName :  new FormControl('',[
    Validators.required,
    Validators.maxLength(10)
  ]),
  lastName:new FormControl('',[
    Validators.required,
    Validators.pattern("[a-zA-Z]*")
  ]),
  gender:new FormControl('',[
    Validators.required,
   
    
  ]),
  contactno:new FormControl('',[
    Validators.required,
    Validators.minLength(10),
    Validators.pattern("[0-9]+")
  ]),
  password:new FormControl('',[
    Validators.required,
    Validators.pattern("(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[~`!@#$%^&*()_-{}<>,./?:;'+=]).{8,16}")
  ]),

  confirmpassword:new FormControl('',[Validators.required,Validators.pattern("(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[~`!@#$%^&*()_-{}<>,./?:;'+=]).{8,16}")
]),

  empid:new FormControl('',[Validators.required,
  Validators.minLength(4),
  Validators.maxLength(4),
  Validators.pattern("[0-9]+")
  ])
}
)






} 

  ngOnInit(){
   // if(localStorage.getItem("json")!=null){
      //var update=JSON.parse(localStorage.getItem("json"));
    if(this.serviceFormData.getData()){
      this.update=JSON.parse(this.serviceFormData.getData());

      this.registerForm.patchValue({
        firstName:this.update.firstName,
        lastName:this.update.lastName,
        gender:this.update.gender,
        contactno:this.update.contactno,
        password:this.update.password,
        empid:this.update.empid
      });
    }
    }

  }
  


