import { BrowserModule } from '@angular/platform-browser';
import { NgModule, Component } from '@angular/core';

import { AppComponent } from './app.component';
import { FormPageComponent } from './formpage/formpage.component';
import { FormsModule } from '@angular/forms'
import { RouterModule, Routes } from '@angular/router';
import { ShowComponent } from './show/show.component';
import { ReactiveFormsModule } from '@angular/forms';
import { StoreService} from './store.service';

const Routes=[
  {
    path:"formpage",
    component:FormPageComponent
  },
  {
    path:"show",
    component: ShowComponent
  }
]

@NgModule({
  declarations: [
    AppComponent,
    FormPageComponent,
    ShowComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forRoot(Routes)
  ],
  providers: [StoreService],
  bootstrap: [AppComponent]
})
export class AppModule {
  

 
}